
<!-- Data list view starts -->
                <section id="data-list-view" class="data-list-view-header">

                    <!-- DataTable starts -->
                    <div class="table-responsive">
                        <?php $this->load->view('flash');?>
                        <table class="table data-list-view">
                            <thead style="border: 1px solid #0000000d;">
                                <tr class="earning-tab" style="background: white;">
                                    <th><input type="checkbox" class="vs-checkbox--input" value=""></th>
                                    <th>Order ID</th>
                                    <th>Status</th>
                                    <th>Date & Time</th>
                                    <th>Product</th>
                                    <th style="background: #70bbda1f;">Amount</th>
                                    <th style="background-color: #ffff001f;">Commision</th>
                                    <th style="background-color: #ffff0036;">Net Amount</th>
                                    <th style="background-color: #ffff0036;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($data as $key => $value) {
                                    $order = wc_get_order( $value['order_id']);
                                    $pid = $value['pid'];
                                    $price = get_post_meta($pid, '_price',true);
                                    $com = get_post_meta($pid, 'sp_commision',true);

                                    ?>
                                <tr role="row" class="odd" style="background: white;">
                                    <td class=""><input type="checkbox" class="vs-checkbox--input" value=""></td>
                                    <td style="color: #0000004f;"><a href=""><strong><?= $value['order_id'] ?></strong></a></td>
                                    <td><span class="status status-<?= $order->get_status(); ?>"><?= ucfirst ($order->get_status()); ?></span></td>
                                    <td>
                                        <?php
                                        echo date("d/m/Y H:i A", strtotime($order->get_date_created()));  
                                        ?></td>
                                    <td class="product-date"><div class="profile-img"><img src="<?= get_the_post_thumbnail_url($pid); ?>"></div><h6><?= get_the_title($pid); ?></h6></td>
                                    <td style="background-color: #70bbda1f;"><?= $price ?></td>
                                    <td style="background-color: #ffff001f ;"><?= $com ?></td> 
                                    <td style="background-color: #ffff0036;"><?= $price-$com ?></td> 
                                    <td style="background-color: #ffff0036;"><?php
                                    if($order->get_status() == 'recived')
                                    {
                                        ?><a href="<?= base_url('admin/admin/myorder'); ?>?open=<?= $value['order_id']; ?>" >Open</a><?php
                                    }
                                     ?></td> 
                                    }
                                </tr>
                                <?php
                                }
                                ?>
                                <!-- <div class="link-panal">
                                    <ul>
                                        <li><a href="">Invoice</a></li>
                                        <li><a href="">Archive</a></li>
                                    </ul>
                                </div> -->
                            </tbody>
                        </table>
                    </div>
                    <!-- DataTable ends -->
                </section>
                <!-- Data list view end -->