
<!-- Data list view starts -->
                <section id="data-list-view" class="data-list-view-header">

                    <!-- DataTable starts -->
                    <div class="table-responsive">
                        <?php $this->load->view('flash');?>
                        <table class="table data-list-view">
                            <thead style="border: 1px solid #0000000d;">
                                <tr style="background: white;">
                                    <th><input type="checkbox" class="vs-checkbox--input" value=""></th>
                                    <th>#</th>
                                    <th>Invoice No</th>
                                    <th>Title</th>
                                    <th >Amount</th>
                                    <th >Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                   <?php
                                   foreach ($data as $key => $value) {
                                       ?>
                                <tr role="row" class="odd" style="background: white;">
                                    <td class=""><input type="checkbox" class="vs-checkbox--input" value=""></td>
                                    
                                    <td style="color: #0000ffd4;font-weight: 900;"><?= $key+1;?></td>
                                    <td class="product-date"><?= $value['ID'] ?></td>
                                    <td class="product-buyer"><strong><?= $value['post_title'] ?></strong></td>
                                    <td><?= get_post_meta($value['ID'], 'amount',true); ?></td>
                                    <td><?= date("d-m-Y", strtotime($value['post_date']));  ?></td>     
                                </tr>
                                <?php
                                   }
                                   ?>
                                <!-- <div class="link-panal">
                                    <ul>
                                        <li><a href="">Invoice</a></li>
                                        <li><a href="">Archive</a></li>
                                    </ul>
                                </div> -->
                            </tbody>
                        </table>
                    </div>
                    <!-- DataTable ends -->
                </section>
                <!-- Data list view end -->