<!-- BEGIN: Main Menu-->
<?php 
$url = get_assets_url();
$user = $_SESSION['knet_login']->data;
$ppack = get_user_meta($user->ID,'ppack',true);
?>
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="../../../html/ltr/vertical-menu-template/index.html">
            <img src="<?= ot_get_option( 'sticky_logo', $url.'img/logo2.png' ) ?>" width="110" height="48" alt="" class="logo_sticky">
                    </a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <?php
                if ($ppack) {
                    ?>
                <li class=" nav-item"><a href="<?= base_url('admin/admin'); ?>"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span><span class="badge badge badge-warning badge-pill float-right mr-2 d-none">2</span></a>
                </li>
                <li class=" nav-item"><a href="<?= base_url('admin/admin/chat'); ?>"><i class="feather icon-message-square"></i><span class="menu-title" data-i18n="Dashboard">Chat</span><span class="badge badge badge-warning badge-pill float-right mr-2 d-none">2</span></a>
                </li>
                <li class=" nav-item"><a href="#"><i class="feather icon-users"></i><span class="menu-title" data-i18n="Ecommerce">Products </span></a>
                    <ul class="menu-content">
                        <li><a href="<?= base_url('admin/products/create');?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Shop">Add Product</span></a>
                        </li>
                        <li><a href="<?= base_url('admin/products/all');?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Details">Manage Products</span></a>
                        </li>
                    </ul>
                </li>
                <?php
                }
                ?>
                <li class=" nav-item"><a href="<?= base_url('admin/admin/subscription'); ?>"><i class="feather icon-message-square"></i><span class="menu-title" data-i18n="Dashboard">Subscription</span><span class="badge badge badge-warning badge-pill float-right mr-2 d-none">2</span></a>
                </li>

                <li class=" nav-item"><a href="<?= base_url('admin/admin/profile'); ?>"><i class="feather icon-message-square"></i><span class="menu-title" data-i18n="Dashboard">Profile</span><span class="badge badge badge-warning badge-pill float-right mr-2 d-none">2</span></a>
                </li>
                <li class=" nav-item"><a href="<?= base_url('admin/admin/myorder'); ?>"><i class="feather icon-user"></i><span class="menu-title" data-i18n="Dashboard">My Orders</span><span class="badge badge badge-warning badge-pill float-right mr-2 d-none">2</span></a>

                <li class=" nav-item"><a href="<?= base_url('admin/admin/payments'); ?>"><i class="feather icon-user"></i><span class="menu-title" data-i18n="Dashboard">Payments</span><span class="badge badge badge-warning badge-pill float-right mr-2 d-none">2</span></a>
                </li>
            </ul>
        </div>
    </div>